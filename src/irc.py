import socket


class Irc:
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def __init__(self, server, port, channel, nick):
        self.server = server
        self.port = port
        self.channel = channel
        self.nick = nick
        return

    def decode(self, string):
        string = string.decode("utf-8")
        string = string.strip('\r\n')
        return string

    def encode(self, string):
        string = bytes(string, "utf-8")
        return string

    def send(self, msg):
        msg = "PRIVMSG " + self.channel + " :" + msg + "\r\n"
        msg = self.encode(msg)
        self.socket.send(msg)
        print("..............", msg)
        return

    def message(self, name, msg):
        msg = "PRIVMSG " + name + " :" + msg + "\r\n"
        msg = self.encode(msg)
        self.socket.send(msg)
        return

    def raw_send(self, msg):
        msg = self.encode(msg + "\r\n")
        self.socket.send(msg)
        return

    def ping(self):
        self.socket.send(bytes("PONG :pings\r\n", "utf-8"))
        return

    def irc_connect(self):
        user = "USER " + (self.nick + " ") * 3 + ":Totally not a bot\n"
        nick = "NICK " + self.nick + "\n"
        channel = "JOIN " + self.channel + "\n"
        self.socket.connect((self.server, self.port))
        self.socket.send(bytes(user, "utf-8"))
        self.socket.send(bytes(nick, "utf-8"))
        self.socket.send(bytes(channel, "utf-8"))
        return

    def recv(self):
        msg = self.socket.recv(2048)
        msg = self.decode(msg)
        return msg
