#!/usr/bin/env python3

from bot import Bot
from threading import Thread
import time
import sys
import kivy
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.clock import Clock
from string import printable
from time import sleep
from cmds.config import *


def start_bot():
    bot.irc_connect()
    bot.send_message("NickServ", "IDENTIFY " + bot_nick + " " + bot_password)


start_bot()


class RootWidget(BoxLayout):
    def __init__(self):
        BoxLayout.__init__(self)
        self.n = 0
        self.close = False
        self.thread = Thread(target=self.receive_message)
        self.thread.start()

    def receive_message(self):
        while not self.close:
            if not self.close:
                self.run_bot(bot.recv())

    def input(self):
        a = self.ids.msgin.text
        if a != "":
            if a[0] == "$":
                bot.irc_raw_message(a[1:])
                self.insert("Raw IRC msg: " + a[1:])
            else:
                bot.send(a)
                self.insert("IRC msg: " + a)
        self.ids.msgin.text = ""

    def run_bot(self, msg):
        notcs_msg = msg.lower()
        if notcs_msg.find("ping :") != -1:
            bot.ping()
            self.insert("Ping answered")
        else:
            msg_i = msg.find("PRIVMSG " + irc_channel)
            if msg_i == -1:
                for msg in msg.split("\n"):
                    if msg != "":
                        self.insert(msg)
                return
            from_name = msg.split("!")[0][1:]
            msg = msg[msg_i + len("PRIVMSG " + irc_channel) + 1:]
            for m in msg.split("\n"):
                if m != "":
                    self.insert(from_name + ": " + m)
            ret_msg = bot.get_msg_response(from_name, msg)
            if ret_msg is None:
                return
            ret_msg = ret_msg.split("\n")
            for line in ret_msg:
                bot.send(line)

    def insert(self, msg):
        self.ids.lv.item_strings.append(msg)
        if len(self.ids.lv.item_strings) > 20:
            self.ids.lv.item_strings.pop(1)


class FluffiehBotApp(App):
    pass

app = FluffiehBotApp()
app.run()
