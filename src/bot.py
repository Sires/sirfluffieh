import re
from irc import Irc
from botapi import BotApi


class Bot(Irc, BotApi):
    def __init__(self, server, port, channel, nick):
        BotApi.__init__(self)
        self.server = server
        self.port = port
        self.channel = channel
        self.nick = nick

    def send_message(self, channel, message):
        self.message(channel, message)
