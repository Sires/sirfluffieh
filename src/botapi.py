from threading import Thread
from time import sleep


class BotApi:
    commands = []

    def __init__(self, default_locked_msg="..", activation_flags=None):
            if activation_flags is None:
                self.activation_flags = {"default": self._pass}
            else:
                self.activation_flags = activation_flags
            self.default_locked_msg = default_locked_msg

    def get_msg_response(self, from_name, msg):
        command = None
        i = None
        params = None
        for index, cmd in enumerate(self.commands):
            act_flag = self.activation_flags[cmd["activation_flag"]]
            activated, param_index = act_flag(cmd["name"], from_name, msg)
            if activated:
                command = cmd
                i = index
                params = msg[param_index:]
                break
        if command is None:
            return
        else:
            if command["unlocked"]:
                if command["max_repeat"] == command["count"]:
                    self.commands[i]["count"] = 1
                    self.commands[i]["unlocked"] = False
                self.commands[i]["count"] += 1
                Thread(target=self.unlock_command,
                       args=(i, command["cooldown"])).start()
                return command["func"](from_name, params)
            else:
                if command["locked_count"]\
                   < command["max_locked_repeat"]:
                    self.commands[i]["locked_count"] += 1
                    if not (command["locked_msg"] is None):
                        return command["locked_msg"]
                    else:
                        return self.default_locked_msg

    def register_command(self, name, func, cooldown=30,
                         locked_msg=None,
                         activation_flag="default",
                         max_repeat=2,
                         max_locked_repeat=2):
        self.commands.append({"name": name,
                              "activation_flag": activation_flag,
                              "func": func,
                              "cooldown": cooldown,
                              "locked_msg": locked_msg,
                              "unlocked": True,
                              "count": 0,
                              "locked_count": 0,
                              "max_repeat": max_repeat,
                              "max_locked_repeat": max_locked_repeat})

    def register_activation_flag(self, name, func):
        self.activation_flags[name] = func

    def unlock_command(self, index, time):
        sleep(time)
        self.commands[index]["count"] = 0
        self.commands[index]["locked_count"] = 0
        self.commands[index]["unlocked"] = True

    def _pass(self):
        pass
