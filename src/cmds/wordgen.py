from .german import generate_german
from .config import bot


def cmd_german(from_name, params):
    n = 12
    if params.isalpha():
        n = min(int(params), 20)
    return generate_german(a=n)


bot.register_command("german", cmd_german, max_repeat=3)
