from .config import *
from kivy.app import App


def default_activation_flag(name, from_name, msg):
    cmd1 = name[0].upper() + name[1:]
    cmd2 = name.lower()
    notcs_msg = msg.lower()
    cmd1 = msg.find(":" + bot_nick[0].upper() + "::" + cmd1) == 0
    cmd2 = msg.find(":" + bot_nick.lower() + " " + cmd2) == 0
    if cmd1:
        return True, len(name) + 5
    elif cmd2:
        return True, len(name) + 11
    else:
        return False, -1


def quit(time):
    sleep(time)
    bot.raw_send("QUIT")
    app = App.get_running_app()
    app.root.closed = True
    app.stop()


def cmd_pong(from_name, params):
    return "Pongg!"


def cmd_hello(from_name, params):
    return "Hello!"


def cmd_unlock(from_name, params):
    if from_name == "" + owner + "" and params.isnumeric():
        bot.unlock_command(int(params), 0)
        return "Yes sir."
    else:
        bot.send("" + owner + "", from_name + " tried to unlock a command")
        return owner + " will be warned about you."


def cmd_quit(from_name, params):
    if from_name == "" + owner + "":
        Thread(target=quit, args=(1,)).start()
        return "Bye bye"
    else:
        bot.send("" + owner + "", from_name + " tried to make me quit")
        return owner + " will be warned about you."


def cmd_reg_tmpcmd(from_name, params):
    p = params.split(" ")
    name = p[0]
    repeat = p[1]
    rest = " ".join(p[2:])
    if from_name == "" + owner + "":
        bot.register_command(name, lambda x, y: rest, max_repeat=repeat)
        print(bot.commands[-1])
        return name + " registered as " + str(len(bot.commands) - 1)
    else:
        bot.send("" + owner + "", from_name + " tried to register a temporary command")
        return owner + " will be warned about you."


bot.register_activation_flag("default", default_activation_flag)
bot.register_command("ping", cmd_pong, max_repeat=1)
bot.register_command("hello", cmd_hello, max_repeat=6)
bot.register_command("unlock", cmd_unlock, max_repeat=3)
bot.register_command("quit", cmd_quit, max_repeat=1)
bot.register_command("get out", cmd_quit, max_repeat=1)
bot.register_command("tmpcmd register", cmd_reg_tmpcmd, max_repeat=1)
