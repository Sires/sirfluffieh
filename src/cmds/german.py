from .config import *
import random


def generate_german(a=12):
    words = "warum bist du rot".split(" ")
    words.extend("ich brauche etwa was du hast".split(" "))
    words.extend("oh ich glaub ich muss erst wieder wilde tomaten finden und anbauen".split(" "))
    words.extend("ich wollte heute mein Baum vergrãern hab aber nicht mehr viel Holz".split(" "))
    words.extend("ich hab mir ne machine gebaut, die es produziert, ist aber langsam".split(" "))
    words.extend("ja, aber lass das heute abend machen, ich muss meine wohnung aufräumen".split(" "))
    words.extend("willst du mein versteck sehen ich bin ein berliner".split(" "))
    wordsn = random.randint(a, a)
    sentence = ""
    lastword = ""
    nextword = ""
    for i in range(0, wordsn):
        while nextword == lastword:
            nextword = random.choice(words)
        sentence = sentence + " " + nextword
        lastword = nextword
    return sentence[1:]
